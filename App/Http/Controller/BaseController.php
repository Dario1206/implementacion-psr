<?php

namespace App\Http\Controllers;



class BaseController
{
    public function index()
    {

        $message['data'] = [
            'message' => 'Hello word!',
            'error' => false
        ];

        var_dump($message);
    }

    /**
     * @param $detail
     **/
    public static function detail(String $detail)
    {
        $message['data'] = [
            'message' => $detail,
            'error' => false
        ];

        var_dump($message);
    }
}