<?php
namespace App\Services\Http;
use GuzzleHttp\Client;

class ApiService
{
    private $res;

    public function __construct(String $url)
    {
        $client = new Client();
        $res = $client->request('GET', $url, [
            'auth' => ['', '']
        ]);
        $this->res = $res;

    }
    public  function getBody()
    {
        $result['body'] = [
            'data'=>$this->res->getBody(),
        ];

        return $result;
    }
    public  function getMessage()
    {
        $result['message'] = [
            'status'=>$this->res->getStatusCode(),
            'header'=>$this->res->getHeader('content-type')[0]
        ];

        return $result;

    }
}